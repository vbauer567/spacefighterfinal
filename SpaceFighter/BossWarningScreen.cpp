
#include <string>
#include "BossWarningScreen.h"
#include "GameplayScreen.h"
#include "MainMenuScreen.h"



BossWarningScreen::BossWarningScreen()
{
	m_pTexture = nullptr;
	
	SetTransitionInTime(1.0f); 
	SetTransitionOutTime(0.5f);

	Show(); 
}

void BossWarningScreen::LoadContent(ResourceManager* pResourceManager)
{
	// loads the boss warning as a texture
	m_pTexture = pResourceManager->Load<Texture>("Textures\\bossWarning.png");
	m_texturePosition = Game::GetScreenCenter();// -Vector2::UNIT_Y * 150;

	warningload = false; 
}

void BossWarningScreen::Update(GameTime* pGameTime)
{
	if (warningload == false) { pGameTime->SetTimeMarker(); warningload = true; }
	double time = pGameTime->GetTotalTime() - pGameTime->GetTimeMarker();
	if (time >=5) {
		WarningScreenTimeOut(this);
	}

	MenuScreen::Update(pGameTime);
}

void BossWarningScreen::Draw(SpriteBatch* pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}

// transitions out of this screen
void BossWarningScreen::WarningScreenTimeOut(MenuScreen* pScreen)
{
	int level = 1;
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen(level));
}
