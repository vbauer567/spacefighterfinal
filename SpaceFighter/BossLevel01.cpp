#include "BossLevel01.h"
#include "BioEnemyShip.h"
#include "BossShip.h" //inlcudes bossship header file


void BossLevel01::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexTure = pResourceManager->Load<Texture>("Textures\\Boss.png"); //Loads boss png into level


	double xPositions = .5;

	double delays = 0;

	float delay = 2.0; // start delay
	Vector2 position;

	for (int i = 0; i < 1; i++)
	{
		
		delay += delays;
		position.Set(xPositions * Game::GetScreenWidth(), -pTexTure->GetCenter().Y * .05);

		BossShip* pBoss = new BossShip();
		pBoss->SetTexture(pTexTure);
		pBoss->SetCurrentLevel(this);
		pBoss->Initialize(position, (float)delay);
		AddGameObject(pBoss);
		
		
	}

	Level::LoadContent(pResourceManager);

}
