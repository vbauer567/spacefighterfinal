
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "BossLevel01.h"
#include "MainMenuScreen.h"
#include "BossWarningScreen.h"

GameplayScreen::GameplayScreen(const int levelIndex)
{
	m_pLevel = nullptr;
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); LevelCase = 0;  break; // Added a LevelCase field so LoadNextScreen() knows which screen to load
	case 1: m_pLevel = new BossLevel01(); LevelCase = 1; break;
	}


	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
	
	m_screenLoaded = false;
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(GameTime *pGameTime)
{

	// Sets the TimeMarker when the screen is loaded
	if (m_screenLoaded == false) { pGameTime->SetTimeMarker(); m_screenLoaded = true; }
	// logs the amount of time since TimeMarker was set
	double time = pGameTime->GetTotalTime() - pGameTime->GetTimeMarker();

	// test for LevelCase for when the screen times out 
	if (LevelCase == 0) {
		// loads the boss warning after the level time has elapsed
		if (time >= 27) { LoadNextScreen(); }
		m_pLevel->Update(pGameTime);
	}
	else if (LevelCase == 1) {
		if (time >= 100) { LoadNextScreen(); }
		m_pLevel->Update(pGameTime);
	}

}

void GameplayScreen::LoadNextScreen()
{
	// test for LevelCase for which sceen should be loaded
	if (LevelCase == 0) {
		GetScreenManager()->AddScreen(new BossWarningScreen());
	}
	else if (LevelCase == 1) {
		GetScreenManager()->AddScreen(new MainMenuScreen());
	}
	
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
