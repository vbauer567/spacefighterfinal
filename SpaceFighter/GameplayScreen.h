
#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class Level;

class GameplayScreen : public Screen
{

public:

	GameplayScreen(const int levelIndex = 0);

	virtual ~GameplayScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void HandleInput(const InputState *pInput);

	virtual void Update(GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void LoadNextScreen();


private:

	Level *m_pLevel;
	int LevelCase;
		
	bool m_screenLoaded;

};
