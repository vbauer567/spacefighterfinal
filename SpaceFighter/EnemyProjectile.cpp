#include "EnemyProjectile.h"
/**/
Texture* EnemyProjectile::s_pTexture = nullptr;


// -------------------------- NOTE: NOT TESTED ----------------------------

// Projectile behavior
EnemyProjectile::EnemyProjectile()
{
	SetSpeed(800); // Projectile speed
	SetDamage(1); // Projectile damage
	SetDirection(Vector2::UNIT_Y); // Projectile direction (down)
	SetCollisionRadius(9); // Collision radius (size of bullet)

	m_drawnByLevel = true;
}

// Update projectile
void EnemyProjectile::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		TranslatePosition(translation);

		Vector2 position = GetPosition();
		Vector2 size = s_pTexture->GetSize();

		if (position.Y < -size.Y) Deactivate();
		else if (position.X < -size.X) Deactivate();
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}

	GameObject::Update(pGameTime);
}

// Draw projectile
void EnemyProjectile::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

// Active projectile
void EnemyProjectile::Activate(const Vector2& position, bool wasShotByEnemy)
{
	m_wasShotByEnemy = wasShotByEnemy;
	SetPosition(position);

	GameObject::Activate();
}

std::string EnemyProjectile::ToString() const
{
	return ((WasShotByEnemy()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType EnemyProjectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByEnemy() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}


/**/