#pragma once
/**/
#include "KatanaEngine.h"
#include "GameObject.h"

class EnemyProjectile : public GameObject
{

public:

	// Constructor and deconstructor
	EnemyProjectile();
	virtual ~EnemyProjectile() { }

	// Set texture
	static void SetTexture(Texture* pTexture) { s_pTexture = pTexture; }

	// Update projectile
	virtual void Update(const GameTime* pGameTime);

	// Draw projectile
	virtual void Draw(SpriteBatch* pSpriteBatch);

	// Activate projectile
	virtual void Activate(const Vector2& position, bool wasShotByEnemy = true);

	// Get damage value
	virtual float GetDamage() const { return m_damage; }

	virtual std::string ToString() const;

	// Get type of collision
	virtual CollisionType GetCollisionType() const;

	// See if object is drawn by level
	virtual bool IsDrawnByLevel() const { return m_drawnByLevel; }

	virtual void SetManualDraw(const bool drawManually = true) { m_drawnByLevel = !drawManually; }


protected:

	// Set speed of projectile
	virtual void SetSpeed(const float speed) { m_speed = speed; }

	// Set damage value of projectile
	virtual void SetDamage(const float damage) { m_damage = damage; }

	// Set the direction the projectile moves in
	virtual void SetDirection(const Vector2 direction) { m_direction = direction; }

	// Set the speed of the projectile
	virtual float GetSpeed() const { return m_speed; }

	// Get the direction
	virtual Vector2& GetDirection() { return m_direction; }

	// See if player was hit by projectile
	virtual bool WasShotByEnemy() const { return m_wasShotByEnemy; }

	// Get type of collision
	virtual CollisionType GetProjectileType() const { return CollisionType::PROJECTILE; }

	// Get projectile type string
	virtual std::string GetProjectileTypeString() const { return "Projectile"; }


private:

	static Texture* s_pTexture;

	float m_speed;
	float m_damage;

	Vector2 m_direction;

	bool m_wasShotByEnemy;

	bool m_drawnByLevel;
};

/**/