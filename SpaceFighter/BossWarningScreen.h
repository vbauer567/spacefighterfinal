#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class BossWarningScreen : public MenuScreen
{

public:

	BossWarningScreen();

	virtual ~BossWarningScreen() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Update(GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


	virtual void SetQuitFlag() { m_isQuittingGame = true; }

	virtual bool IsQuittingGame() { return m_isQuittingGame; }

	virtual void WarningScreenTimeOut(MenuScreen* pScreen);

private:
	bool warningload;

	Texture* m_pTexture;

	Vector2 m_texturePosition;

	bool m_isQuittingGame = false;

};

