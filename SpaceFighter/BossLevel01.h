#pragma once

#include "Level.h"

class BossLevel01 : public Level
{

public:

	BossLevel01() { }

	virtual ~BossLevel01() { }

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void UnloadContent() { }

};