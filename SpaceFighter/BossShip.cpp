#include "BossShip.h"


BossShip::BossShip()
{
	SetSpeed(80);
	SetMaxHitPoints(20);
	SetCollisionRadius(80);
	m_isEntering = true;


}


void BossShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		
		if(m_isEntering == true)
		{
			float y = 1 ; // unused
			y *= 50; //unused
			TranslatePosition(0, 150);
			m_isEntering = false;
		}
		//not doing what i wanted but boss is working movement

		if (m_isEntering == false)
		{
			float x = sin(pGameTime->GetTotalTime());
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 3.5f;
			TranslatePosition(x, 0);
			if (!IsOnScreen()) Deactivate();
		}
		

		
	}

	EnemyShip::Update(pGameTime);
}


void BossShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}