
#pragma once

#include "EnemyShip.h"

class BossShip : public EnemyShip
{

public:

	BossShip();
	
	virtual ~BossShip() { };

	virtual void SetTexture(Texture* pTexture) { m_pTexture = pTexture; };

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw( SpriteBatch* pSpriteBatch );

private:

	Texture* m_pTexture = nullptr;

	bool m_isEntering = true;


};




